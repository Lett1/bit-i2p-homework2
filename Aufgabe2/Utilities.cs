﻿//-----------------------------------------------------------------------
// <copyright file="Utilities.cs" company="CompanyName">
//     Copyright (c) LettSoft.
// </copyright>
//-----------------------------------------------------------------------
namespace Aufgabe2
{
    using System;

    /// <summary>
    /// Miscellanious functions that can be reused in other projects.
    /// </summary>
    public static class Utilities
    {
        /// <summary>
        /// Generates lorem ipsum of specified length.
        /// </summary>
        /// <returns>Some lorem ipsum.</returns>
        /// <param name="r">An instance of System.Random.</param>
        /// <param name="maxWords">How many words to generate.</param>
        public static string GenerateRandomText(Random r, int maxWords)
        {
            string text = string.Empty;
            string[] loremIpsumWords = { "lorem", "ipsum", "dolor", "sit", "amet", "conseteur", "sadipscing", "elitr", "sed" };

            for (int i = 0; i < maxWords; i++)
            {
                text += loremIpsumWords[r.Next(0, loremIpsumWords.Length)];

                if (i < maxWords)
                {
                    text += " ";
                }
                else
                {
                    text += ".";
                }
            }

            return text;
        }

        /// <summary>
        /// Splits a string into a string[] by breaking it after a given length.
        /// </summary>
        /// <returns>A string[] containing the lines.</returns>
        /// <param name="text">The Text.</param>
        /// <param name="maxlinelength">Maximum length of a line.</param>
        public static string[] SplitTextByLineLength(string text, int maxlinelength)
        {
            int lineCount = (int)Math.Ceiling((double)text.Length / maxlinelength);
            string[] lines = new string[lineCount];

            for (int i = 0; i < lines.Length; i++)
            {
                lines[i] = text.Substring(i * maxlinelength, Math.Min(maxlinelength, text.Length - (i * maxlinelength)));
            }

            return lines;
        }
       
        /// <summary>
        /// Truncates a string after a specific length and pads it with another string.
        /// E.g. "Hello World" -> "Hello W..."
        /// </summary>
        /// <returns>The truncated string.</returns>
        /// <param name="text">The string to truncate.</param>
        /// <param name="maxLength">Maximum string length.</param>
        /// <param name="paddingChar">The char to pad the string with.</param>
        /// <param name="paddingLength">How many characters to pad onto the string.</param>
        public static string TruncateString(string text, int maxLength, char paddingChar = '.', int paddingLength = 3)
        {
            if (text.Length < maxLength)
            {
                return text;
            }
            else
            {
                return text.Substring(0, Math.Max(0, maxLength - paddingLength)) + new string(paddingChar, Clamp(1, paddingLength, maxLength));
            }
        }

        /// <summary>
        /// Contrains a value into a given range.
        /// </summary>
        /// <returns>The clamped value.</returns>
        /// <param name="min">Lower bound.</param>
        /// <param name="max">Upper bound.</param>
        /// <param name="value">The value to clamp.</param>
        public static int Clamp(int min, int max, int value)
        {
            if (value < min)
            {
                return min;
            }
            else if (value > max)
            {
                return max;
            }
            else
            {
                return value;
            }
        } 

        /// <summary>
        /// Asks the user a yes/no question. (y/n) is automatically added to the string.
        /// </summary>
        /// <returns><c>true</c>, if yes was answered, <c>false</c> otherwise.</returns>
        /// <param name="question">Main text of the question.</param>
        public static bool AskForBoolean(string question)
        {
            Console.Write(question + " (y/n)");

            while (true)
            {
                string input = Console.ReadLine();

                switch (input.ToLower())
                {
                    case "y":
                    case "yes":
                        return true;
                    case "n":
                    case "no":
                        return false;
                }

                Console.Write("Please answer y or n:");
            }
        }

        /// <summary>
        /// Asks the user for a single character.
        /// </summary>
        /// <returns>The entered characater.</returns>
        /// <param name="description">The use of the character.</param>
        public static char AskForChar(string description)
        {
            Console.WriteLine($"Please a new single character for {description}.");

            string input;

            while (true)
            {
                Console.Write("=>");
                input = Console.ReadLine();

                if (string.IsNullOrWhiteSpace(input))
                {
                    Console.WriteLine($"Please enter a character.");
                    continue;
                }
                else
                {
                    if (input.Length > 1)
                    {
                        Console.WriteLine("Please enter a single character only.");
                        continue;
                    }
                    else
                    {
                        return input[0];
                    }
                }
            } 
        }

        /// <summary>
        /// Allows the user to select one of the 16 console colors.
        /// </summary>
        /// <returns>The selected color.</returns>
        /// <param name="description">The use of the color.</param>
        /// <param name="oldColor">The old color.</param>
        public static ConsoleColor AskForColor(string description, ConsoleColor oldColor)
        {
            string[] colorNames = 
            {
                "Black", "Dark Blue", "Dark Green", "Dark Cyan", "Dark Red", "Dark Magenta", "Dark Yellow",
                "Gray", "Dark Gray", "Blue", "Green", "Cyan", "Red", "Magenta", "Yellow", "White"
            };

            int index = 0;

            Console.CursorVisible = false;

            while (true)
            {
                Console.BackgroundColor = ConsoleColor.Black; // Make the console clear to black
                Console.Clear();

                DrawTextBar($"Please select a new color for {description}", backgroundColor: ConsoleColor.Blue);
                Console.WriteLine();

                for (int i = 0; i < 16; i++)
                {
                    Console.ForegroundColor = (ConsoleColor)i;

                    if (i == 0)
                    {
                        Console.BackgroundColor = ConsoleColor.White;
                    }

                    if (i == index)
                    {
                        Console.Write("=>");
                    }

                    Console.Write("███");
                    Console.WriteLine(colorNames[i]);
                   
                    Console.BackgroundColor = ConsoleColor.Black;
                    Console.ForegroundColor = ConsoleColor.White;
                }

                ConsoleKeyInfo input = Console.ReadKey(true);

                switch (input.Key)
                {
                    case ConsoleKey.DownArrow:
                        if (index < 16 - 1)
                        {
                            index++;
                        }

                        break;
                    case ConsoleKey.UpArrow:
                        if (index > 0)
                        {
                            index--;
                        }

                        break;
                    case ConsoleKey.Enter:
                        return (ConsoleColor)index;
                    case ConsoleKey.Escape:
                        return oldColor;
                }
            }
        }

        /// <summary>
        /// Allows the user to enter a string. Returns a default value of nothing was entered.
        /// </summary>
        /// <returns>The entered string.</returns>
        /// <param name="description">The use of the string.</param>
        /// <param name="defaultValue">Default value.</param>
        public static string AskForString(string description, string defaultValue = null)
        {
            Console.WriteLine($"Please enter a new value for {description}");
            Console.WriteLine("Enter nothing to keep the current value.");

            string input;

            while (true)
            {
                Console.Write("=>");
                input = Console.ReadLine();

                if (string.IsNullOrWhiteSpace(input))
                {
                    return defaultValue;
                }
                else
                {
                    return input;
                }
            } 
        }

        /// <summary>
        /// Allows the user to enter a number in the specified range.
        /// </summary>
        /// <returns>The for number.</returns>
        /// <param name="description">The use of the number.</param>
        /// <param name="min">Minimum of the number.</param>
        /// <param name="max">Maximum of the number.</param>
        public static int AskForNumber(string description, int min = 0, int max = 1000)
        {
            Console.WriteLine($"Please enter a new value for {description}.");
            Console.WriteLine($"Accepted value is a number between {min} and {max}.");

            int number = 0;

            while (true)
            {
                Console.Write("=>");
                string input = Console.ReadLine();

                if (string.IsNullOrWhiteSpace(input))
                {
                    Console.WriteLine($"Please enter a number between {min} and {max}.");
                    continue;
                }

                if (int.TryParse(input, out number))
                {
                   if (number < min || number > max)
                    {
                        Console.WriteLine($"Please enter a number between {min} and {max}.");
                        continue;
                    }
                    else
                    {
                        return number;
                    }
                }
                else
                {
                    Console.WriteLine($"\'{input}\' is not a number!");
                }
            } 
        }

        /// <summary>
        /// Waits for the supplied keys.
        /// </summary>
        /// <param name="keys">One or more ConsoleKeys.</param>
        public static void WaitForKeys(params ConsoleKey[] keys)
        {
            while (true)
            {
                ConsoleKey key = Console.ReadKey(true).Key;
                if (Array.IndexOf(keys, key) > -1)
                {
                    break;
                }
            }
        }

        /// <summary>
        /// Shows a menu and allows the user to choose an entry
        /// </summary>
        /// <returns>int representing the chosen option.</returns>
        /// <param name="title">Menu title.</param>
        /// <param name="items">Menu items.</param>
        /// <param name="descriptions">Item descriptions</param>
        public static int ShowMenu(string title, string[] items, string[] descriptions = null)
        {
            int index = 0;

            Console.CursorVisible = false;

            while (true)
            {
                Console.BackgroundColor = ConsoleColor.Black; // Make the console clear to black
                Console.Clear();

                DrawTextBar(title, backgroundColor: ConsoleColor.Blue);
                Console.WriteLine();

                for (int i = 0; i < items.Length; i++)
                {
                    if (i == index)
                    {
                        Console.BackgroundColor = ConsoleColor.White;
                        Console.ForegroundColor = ConsoleColor.Black;
                    }

                    Console.WriteLine(items[i]);
                    if (descriptions != null)
                    {
                        DrawTextBar(descriptions[index], y: Console.WindowHeight - 1);
                    }

                    Console.BackgroundColor = ConsoleColor.Black;
                    Console.ForegroundColor = ConsoleColor.White;
                }

                ConsoleKeyInfo input = Console.ReadKey(true);

                switch (input.Key)
                {
                    case ConsoleKey.DownArrow:
                        if (index < items.Length - 1)
                        {
                            index++;
                        }

                        break;
                    case ConsoleKey.UpArrow:
                        if (index > 0)
                        {
                            index--;
                        }

                        break;
                    case ConsoleKey.Enter:
                        return index;
                    case ConsoleKey.Escape:
                        return -1;
                }
            }
        }

        /// <summary>
        /// Draws a colored bar of text.
        /// </summary>
        /// <param name="title">Content of the bar.</param>
        /// <param name="x">The x coordinate.</param>
        /// <param name="y">The y coordinate.</param>
        /// <param name="foregroundColor">Foreground color.</param>
        /// <param name="backgroundColor">Background color.</param>
        public static void DrawTextBar(string title, int x = 0, int y = 0, ConsoleColor foregroundColor = ConsoleColor.Black, ConsoleColor backgroundColor = ConsoleColor.White)
        {
            ConsoleColor oldForeground = Console.ForegroundColor;
            ConsoleColor oldBackground = Console.BackgroundColor;

            Console.BackgroundColor = backgroundColor;
            Console.ForegroundColor = foregroundColor;

            int oldX = Console.CursorLeft;
            int oldY = Console.CursorTop;

            Console.SetCursorPosition(x, y);
            Console.Write(title);

            Console.SetCursorPosition(oldX, oldY);
            Console.BackgroundColor = oldBackground;
            Console.ForegroundColor = oldForeground;
        }
    }
}
