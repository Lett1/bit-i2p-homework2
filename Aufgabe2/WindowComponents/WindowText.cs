﻿//-----------------------------------------------------------------------
// <copyright file="WindowText.cs" company="CompanyName">
//     Copyright (c) LettSoft.
// </copyright>
//-----------------------------------------------------------------------
namespace Aufgabe2.WindowComponents
{
    using System;

    /// <summary>
    /// Class that holds a string and two ConsoleColor variables to represent colored text in the console.
    /// </summary>
    public class WindowText
    {
        /// <summary>
        /// The text.
        /// </summary>
        private string text;

        /// <summary>
        /// The foreground color of the text.
        /// </summary>
        private ConsoleColor foregroundColor;

        /// <summary>
        /// The background color of the text.
        /// </summary>
        private ConsoleColor backgroundcolor;

        /// <summary>
        /// Initializes a new instance of the <see cref="T:Aufgabe2.WindowComponents.WindowText"/> class.
        /// </summary>
        /// <param name="text">The text to use.</param>
        public WindowText(string text)
        {
            this.Text = text;
            this.ForegroundColor = Console.ForegroundColor;
            this.Backgroundcolor = Console.BackgroundColor;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:Aufgabe2.WindowComponents.WindowText"/> class, copying values from the supplied <see cref="T:Aufgabe2.WindowComponents.WindowText"/> instance.
        /// </summary>
        /// <param name="baseWindowText">Base window text.</param>
        public WindowText(WindowText baseWindowText)
        {
            this.Text = baseWindowText.Text;
            this.ForegroundColor = baseWindowText.ForegroundColor;
            this.Backgroundcolor = baseWindowText.Backgroundcolor;
        }

        /// <summary>
        /// Gets or sets the text value.
        /// </summary>
        /// <value>The text.</value>
        public string Text
        {
            get
            {
                return this.text;
            }

            set
            {
                this.text = value;
            }
        }

        /// <summary>
        /// Gets or sets the color of the foreground.
        /// </summary>
        /// <value>The color of the foreground.</value>
        public ConsoleColor ForegroundColor
        {
            get
            {
                return this.foregroundColor;
            }

            set
            {
                this.foregroundColor = value;
            }
        }

        /// <summary>
        /// Gets or sets the color of the background.
        /// </summary>
        /// <value>The backgroundcolor.</value>
        public ConsoleColor Backgroundcolor
        {
            get
            {
                return this.backgroundcolor;
            }

            set
            {
                this.backgroundcolor = value;
            }
        }

        /// <summary>
        /// Returns a <see cref="T:System.String"/> that represents the current <see cref="T:Aufgabe2.WindowText"/>.
        /// </summary>
        /// <returns>A <see cref="T:System.String"/> that represents the current <see cref="T:Aufgabe2.WindowText"/>.</returns>
        public override string ToString()
        {
            return this.Text;
        }
    }
}
