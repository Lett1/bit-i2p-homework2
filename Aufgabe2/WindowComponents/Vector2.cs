﻿//-----------------------------------------------------------------------
// <copyright file="Vector2.cs" company="CompanyName">
//     Copyright (c) LettSoft.
// </copyright>
//-----------------------------------------------------------------------
namespace Aufgabe2.WindowComponents
{
    using System;

    /// <summary>
    /// Holds two integers representing x and y coordinates.
    /// </summary>
    public class Vector2
    {
        /// <summary>
        /// The x coordinate.
        /// </summary>
        private int x;

        /// <summary>
        /// The y coordinate.
        /// </summary>
        private int y;

        /// <summary>
        /// Initializes a new instance of the <see cref="T:Aufgabe2.WindowComponents.Vector2"/> class.
        /// </summary>
        /// <param name="x">The x coordinate.</param>
        /// <param name="y">The y coordinate.</param>
        public Vector2(int x, int y)
        {
            this.X = x;
            this.Y = y;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:Aufgabe2.WindowComponents.Vector2"/> class, copying values from the supplied <see cref="T:Aufgabe2.WindowComponents.Vector2"/> instance.
        /// </summary>
        /// <param name="baseVector">Base vector.</param>
        public Vector2(Vector2 baseVector)
        {
            this.X = baseVector.X;
            this.Y = baseVector.Y;
        }

        /// <summary>
        /// Gets or sets the x coordinate of the vector.
        /// </summary>
        /// <value>The x coordinate.</value>
        public int X
        {
            get
            {
                return this.x;
            }

            set
            {
                this.x = value;
            }
        }

        /// <summary>
        /// Gets or sets the y coordinate of the vector.
        /// </summary>
        /// <value>The y coordinate.</value>
        public int Y
        {
            get
            {
                return this.y;
            }

            set
            {
                this.y = value;
            }
        }

        /// <summary>
        /// Returns a <see cref="T:System.String"/> that represents the current <see cref="T:Aufgabe2.Vector2"/>.
        /// </summary>
        /// <returns>A <see cref="T:System.String"/> that represents the current <see cref="T:Aufgabe2.Vector2"/>.</returns>
        public override string ToString()
        {
            return string.Format("({0}, {1})", this.X, this.Y);
        }
    }
}
