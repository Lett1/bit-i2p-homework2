﻿//-----------------------------------------------------------------------
// <copyright file="WindowShadow.cs" company="CompanyName">
//     Copyright (c) LettSoft.
// </copyright>
//-----------------------------------------------------------------------
namespace Aufgabe2.WindowComponents
{
    using System;

    /// <summary>
    /// Holds a boolean that represents whether the shadow is visible or not, the character used to draw the shadow and two ConsoleColor variables.
    /// </summary>
    public class WindowShadow
    {
        /// <summary>
        /// Whether the shadow is visible or not.
        /// </summary>
        private bool isVisible;

        /// <summary>
        /// The character used to draw the shadow.
        /// </summary>
        private char character;

        /// <summary>
        /// The color of the foreground.
        /// </summary>
        private ConsoleColor foregroundColor;

        /// <summary>
        /// The color of the background.
        /// </summary>
        private ConsoleColor backgroundcolor;

        /// <summary>
        /// Initializes a new instance of the <see cref="T:Aufgabe2.WindowComponents.WindowShadow"/> class.
        /// </summary>
        /// <param name="isVisible">If set to <c>true</c>, the shadow is visible.</param>
        /// <param name="character">The character used to draw the shadow.</param>
        public WindowShadow(bool isVisible, char character)
        {
            this.IsVisible = isVisible;
            this.Character = character;
            this.ForegroundColor = ConsoleColor.DarkGray;
            this.Backgroundcolor = ConsoleColor.DarkGray;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:Aufgabe2.WindowComponents.WindowShadow"/> class, copying the values from the supplied <see cref="T:Aufgabe2.WindowComponents.WindowShadow"/> instance.
        /// </summary>
        /// <param name="baseWindowShadow">Base WindowShadow object.</param>
        public WindowShadow(WindowShadow baseWindowShadow)
        {
            this.IsVisible = baseWindowShadow.IsVisible;
            this.Character = baseWindowShadow.Character;
            this.ForegroundColor = baseWindowShadow.ForegroundColor;
            this.Backgroundcolor = baseWindowShadow.Backgroundcolor;
        }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="T:Aufgabe2.Window.WindowShadow"/> is visible.
        /// </summary>
        /// <value><c>true</c> if is visible; otherwise, <c>false</c>.</value>
        public bool IsVisible
        {
            get
            {
                return this.isVisible;
            }

            set
            {
                this.isVisible = value;
            }
        }

        /// <summary>
        /// Gets or sets the character used for drawing the shadow.
        /// </summary>
        /// <value>The character.</value>
        public char Character
        {
            get
            {
                return this.character;
            }

            set
            {
                this.character = value;
            }
        }

        /// <summary>
        /// Gets or sets the color of the foreground.
        /// </summary>
        /// <value>The color of the foreground.</value>
        public ConsoleColor ForegroundColor
        {
            get
            {
                return this.foregroundColor;
            }

            set
            {
                this.foregroundColor = value;
            }
        }

        /// <summary>
        /// Gets or sets the color of the background.
        /// </summary>
        /// <value>The color of the background.</value>
        public ConsoleColor Backgroundcolor
        {
            get
            {
                return this.backgroundcolor;
            }

            set
            {
                this.backgroundcolor = value;
            }
        }

        /// <summary>
        /// Returns a <see cref="T:System.String"/> that represents the current <see cref="T:Aufgabe2.WindowShadow"/>.
        /// </summary>
        /// <returns>A <see cref="T:System.String"/> that represents the current <see cref="T:Aufgabe2.WindowShadow"/>.</returns>
        public override string ToString()
        {
            return string.Format(
                "[WindowShadow: IsVisible={0}, Character={1}, ForegroundColor={2}, Backgroundcolor={3}]",
                this.IsVisible,
                this.Character,
                this.ForegroundColor,
                this.Backgroundcolor);
        }
    }
}
