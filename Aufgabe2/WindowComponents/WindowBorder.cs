﻿//-----------------------------------------------------------------------
// <copyright file="WindowBorder.cs" company="CompanyName">
//     Copyright (c) LettSoft.
// </copyright>
//-----------------------------------------------------------------------
namespace Aufgabe2.WindowComponents
{
    using System;

    /// <summary>
    /// Holds a single character and two ConsoleColor variables to represent a colored character in the console.
    /// </summary>
    public class WindowBorder
    {
        /// <summary>
        /// The character.
        /// </summary>
        private char character;

        /// <summary>
        /// The color of the foreground.
        /// </summary>
        private ConsoleColor foregroundColor;

        /// <summary>
        /// The color of the background.
        /// </summary>
        private ConsoleColor backgroundcolor;

        /// <summary>
        /// Initializes a new instance of the <see cref="T:Aufgabe2.WindowComponents.WindowBorder"/> class.
        /// </summary>
        /// <param name="character">The character.</param>
        public WindowBorder(char character)
        {
            this.Character = character;
            this.ForegroundColor = Console.ForegroundColor;
            this.Backgroundcolor = Console.BackgroundColor;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:Aufgabe2.WindowComponents.WindowBorder"/> class, copying values from the supplied <see cref="T:Aufgabe2.WindowComponents.WindowBorder"/> instance.
        /// </summary>
        /// <param name="baseWindowBorder">Base window border.</param>
        public WindowBorder(WindowBorder baseWindowBorder)
        {
            this.Character = baseWindowBorder.Character;
            this.ForegroundColor = baseWindowBorder.ForegroundColor;
            this.Backgroundcolor = baseWindowBorder.Backgroundcolor;
        }

        /// <summary>
        /// Gets or sets the character.
        /// </summary>
        /// <value>The character.</value>
        public char Character
        {
            get
            {
                return this.character;
            }

            set
            {
                this.character = value;
            }
        }

        /// <summary>
        /// Gets or sets the color of the foreground.
        /// </summary>
        /// <value>The color of the foreground.</value>
        public ConsoleColor ForegroundColor
        {
            get
            {
                return this.foregroundColor;
            }

            set
            {
                this.foregroundColor = value;
            }
        }

        /// <summary>
        /// Gets or sets the color of the background.
        /// </summary>
        /// <value>The color of the background.</value>
        public ConsoleColor Backgroundcolor
        {
            get
            {
                return this.backgroundcolor;
            }

            set
            {
                this.backgroundcolor = value;
            }
        }

        /// <summary>
        /// Returns a <see cref="T:System.String"/> that represents the current <see cref="T:Aufgabe2.WindowBorder"/>.
        /// </summary>
        /// <returns>A <see cref="T:System.String"/> that represents the current <see cref="T:Aufgabe2.WindowBorder"/>.</returns>
        public override string ToString()
        {
            return string.Format(
                "[WindowBorder: Character={0}, ForegroundColor={1}, Backgroundcolor={2}]",
                 this.Character,
                 this.ForegroundColor,
                 this.Backgroundcolor);
        }
    }
}
