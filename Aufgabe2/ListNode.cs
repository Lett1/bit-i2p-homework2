﻿//-----------------------------------------------------------------------
// <copyright file="ListNode.cs" company="CompanyName">
//     Copyright (c) LettSoft.
// </copyright>
//-----------------------------------------------------------------------
namespace Aufgabe2
{
    /// <summary>
    /// Implements a single node for use in the WindowList structure.
    /// </summary>
    public class ListNode
    {
        /// <summary>
        /// The following node in the linked list.
        /// </summary>
        private ListNode nextNode;

        /// <summary>
        /// Holds the window stored in the node.
        /// </summary>
        private Window value;

        /// <summary>
        /// Initializes a new instance of the ListNode class.
        /// </summary>
        /// <param name="value">The object stored in the node.</param>
        public ListNode(Window value)
        {
            this.Value = value;
        }

        /// <summary>
        /// Gets or sets the next node in the linked list.
        /// </summary>
        public ListNode NextNode
        {
            get
            {
                return this.nextNode;
            }

            set
            {
                this.nextNode = value;
            }
        }

        /// <summary>
        /// Gets or sets the object stored in the node.
        /// </summary>
        public Window Value
        {
            get
            {
                return this.value;
            }

            set
            {
                this.value = value;
            }
        }

        /// <summary>
        /// Returns the string representation of the nodes value.
        /// </summary>
        /// <returns>The nodes value as a string.</returns>
        public override string ToString()
        {
            return this.Value.ToString();
        }
    }
}
