﻿//-----------------------------------------------------------------------
// <copyright file="WindowList.cs" company="CompanyName">
//     Copyright (c) LettSoft.
// </copyright>
//-----------------------------------------------------------------------
namespace Aufgabe2
{
    using System;

    /// <summary>
    /// Implements a linked list to hold a variable amount of Window objects.
    /// </summary>
    public class WindowList
    {
        /// <summary>
        /// The head of the list.
        /// </summary>
        private ListNode firstNode;

        /// <summary>
        /// How many windows are in this list.
        /// </summary>
        private int count;

        /// <summary>
        /// True if the count variable needs to be updated. Set to true whenever new elements are added or deleted.
        /// </summary>
        private bool needsRecount;

        /// <summary>
        /// Initializes a new instance of the <see cref="T:Aufgabe2.WindowList"/> class.
        /// </summary>
        public WindowList()
        {
            this.needsRecount = false;
        }

        /// <summary>
        /// Gets the amount of elements in this list.
        /// </summary>
        /// <value>The amount of elements in this list.</value>
        public int Count 
        {
            get
            {
                if (this.needsRecount)
                {
                    this.count = this.GetLength();
                    this.needsRecount = false;
                }

                return this.count;
            }
        }

        /// <summary>
        /// Swaps two elements of the list by their given indices
        /// </summary>
        /// <param name="first">The first element.</param>
        /// <param name="second">The second element.</param>
        public void SwapElements(int first, int second)
        {
            Window tmp = this.NodeAt(first).Value;
            this.NodeAt(first).Value = this.NodeAt(second).Value;
            this.NodeAt(second).Value = tmp;
        }

        /// <summary>
        /// Adds a new object to the end of the list.
        /// </summary>
        /// <param name="value">The object to add to the list.</param>
        public void Add(Window value)
        {
            if (this.firstNode == null)
            {
                this.firstNode = new ListNode(value);
            }
            else
            {
                ListNode lastNode = this.FindLastNode();
                lastNode.NextNode = new ListNode(value);
            }

            this.needsRecount = true;
        }

        /// <summary>
        /// Removes and returns the object at the given index.
        /// Throws an IndexOutOfRangeException if the index is less than 0 or bigger than the list.
        /// </summary>
        /// <param name="index">Which element to remove</param>
        /// <returns>The removed element.</returns>
        public Window RemoveAt(int index)
        {
            if (index < 0)
            {
                throw new IndexOutOfRangeException("Index must be a positive number");
            }

            if (index > this.GetLength())
            {
                throw new IndexOutOfRangeException("Index was out of range");
            }

            ListNode node = this.firstNode;
            ListNode previous = new ListNode(null);

            while (node.NextNode != null && index > 0)
            {
                previous = node;
                node = node.NextNode;
                index--;
            }

            previous.NextNode = node.NextNode;

            this.needsRecount = true;
            return node.Value;
        }

        /// <summary>
        /// Gets the object of the element at the specified index.
        /// </summary>
        /// <param name="index">Which element to get.</param>
        /// <returns>The specified object.</returns>
        public Window ElementAt(int index)
        {
            if (index < 0)
            {
                throw new IndexOutOfRangeException("Index must be a positive number");
            }

            if (index > this.GetLength())
            {
                throw new IndexOutOfRangeException("Index was out of range");
            }

            ListNode node = this.firstNode;

            while (node.NextNode != null && index > 0)
            {
                node = node.NextNode;
                index--;
            }

            return node.Value;
        }

        /// <summary>
        /// Clears the entire list by setting the first node to null;
        /// </summary>
        public void ClearList()
        {
            this.firstNode = null;
            this.needsRecount = false;
        }

        /// <summary>
        /// Converts the list into an array of objects.
        /// </summary>
        /// <returns>The new array</returns>
        public Window[] ToArray()
        {
            Window[] temp = new Window[this.GetLength()];

            int index = 0;
            ListNode node = this.firstNode;

            while (node != null)
            {
                temp[index] = node.Value;
                node = node.NextNode;
                index++;
            }

            return temp;
        }

        /// <summary>
        /// Counts the number of elements in the list.
        /// </summary>
        /// <returns>The number of elements in the list.</returns>
        private int GetLength()
        {
            int count = 0;
            ListNode node = this.firstNode;

            while (node != null)
            {
                node = node.NextNode;
                count++;
            }

            return count;
        }

        /// <summary>
        /// Finds and returns the last node in the list.
        /// </summary>
        /// <returns>The last node.</returns>
        private ListNode FindLastNode()
        {
            ListNode node = this.firstNode;

            while (node.NextNode != null)
            {
                node = node.NextNode;
            }

            return node;
        }

        /// <summary>
        /// Gets the node at the specified index
        /// </summary>
        /// <returns>The specified node.</returns>
        /// <param name="index">Which node to get.</param>
        private ListNode NodeAt(int index)
        {
            if (index < 0)
            {
                throw new IndexOutOfRangeException("Index must be a positive number");
            }

            if (index > this.GetLength())
            {
                throw new IndexOutOfRangeException("Index was out of range");
            }

            ListNode node = this.firstNode;

            while (node.NextNode != null && index > 0)
            {
                node = node.NextNode;
                index--;
            }

            return node;
        }
    }
}
