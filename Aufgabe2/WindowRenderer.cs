﻿//-----------------------------------------------------------------------
// <copyright file="WindowRenderer.cs" company="CompanyName">
//     Copyright (c) LettSoft.
// </copyright>
//-----------------------------------------------------------------------
namespace Aufgabe2
{
    using System;

    /// <summary>
    /// Implements the ability to render a Window object to the console.
    /// </summary>
    public static class WindowRenderer
    {
        /// <summary>
        /// Draws a window.
        /// </summary>
        /// <param name="window">A Window object.</param>
        public static void DrawWindow(Window window)
        {
            // Assign shorter names for often used variables
            int x = window.Position.X;
            int y = window.Position.Y;
            int width = window.Width;
            int height = window.Height;

            // Draw the shadow by drawing a box offset by 1 in each direction
            if (window.Shadow.IsVisible)
            {
                Console.ForegroundColor = window.Shadow.ForegroundColor;
                Console.BackgroundColor = window.Shadow.Backgroundcolor;
                DrawBox(x + 1, y + 1, width, height, window.Shadow.Character);
            }

            // Draw the main rectangle of the window
            Console.ForegroundColor = window.Content.ForegroundColor;
            Console.BackgroundColor = window.Content.Backgroundcolor;
            DrawFilledBox(x, y, width, height, ' ');

            // Draw the border
            Console.ForegroundColor = window.Border.ForegroundColor;
            Console.BackgroundColor = window.Border.Backgroundcolor;
            DrawBox(x, y, width, height, window.Border.Character);

            // Truncate title string and calculate horizontal offset
            string title = Utilities.TruncateString(window.Title.Text, width - 6);
            title = $" {title} ";
            int titleoffset = (int)Math.Ceiling((width / 2) - (double)(title.Length / 2));

            // Draw the title
            Console.ForegroundColor = window.Title.ForegroundColor;
            Console.BackgroundColor = window.Title.Backgroundcolor;
            DrawString(x + titleoffset, y, title);

            // Truncate content text and split it into lines for wrapping
            string text = Utilities.TruncateString(window.Content.Text, (width - 4) * (height - 4));

            string[] lines = Utilities.SplitTextByLineLength(text, width - 4);

            // Draw each line
            Console.ForegroundColor = window.Content.ForegroundColor;
            Console.BackgroundColor = window.Content.Backgroundcolor;
            for (int i = 0; i < lines.Length; i++)
            {
                DrawString(x + 2, y + i + 2, lines[i]);
            }
        }

        /// <summary>
        /// Draws a string at the selected x and y coordinates.
        /// </summary>
        /// <param name="x">The x coordinate.</param>
        /// <param name="y">The y coordinate.</param>
        /// <param name="text">The string to draw.</param>
        private static void DrawString(int x, int y, string text)
        {
            for (int i = 0; i < text.Length; i++)
            {
                PutCharacter(x + i, y, text[i]);
            }
        }

        /// <summary>
        /// Draws a hollow box of a given width and height at the specified coordinates.
        /// </summary>
        /// <param name="x">The x coordinate.</param>
        /// <param name="y">The y coordinate.</param>
        /// <param name="width">Width of the box.</param>
        /// <param name="height">Height of the box.</param>
        /// <param name="c">The character used to draw the box. Defaults to a blank character.</param>
        private static void DrawBox(int x, int y, int width, int height, char c = ' ')
        {
            // Draw the two horizontal lines
            for (int i = 0; i < width; i++)
            {
                PutCharacter(x + i, y, c);
                PutCharacter(x + i, y + height - 1, c);
            }

            // Draw the two vertical lines
            for (int i = 0; i < height - 1; i++)
            {
                PutCharacter(x, y + i, c);
                PutCharacter(x + width - 1, y + i, c);
            }
        }

        /// <summary>
        /// Draws a filled box of a given width and height at the specified coordinates.
        /// </summary>
        /// <param name="x">The x coordinate.</param>
        /// <param name="y">The y coordinate.</param>
        /// <param name="width">Width of the box.</param>
        /// <param name="height">Height of the box.</param>
        /// <param name="c">The character used to draw the box.</param>
        private static void DrawFilledBox(int x, int y, int width, int height, char c = ' ')
        {
            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    PutCharacter(x + i, y + j, c);
                }
            }
        }

        /// <summary>
        /// Puts a character at the specified coordinates.
        /// Does not attempt to draw outside of the Console boundaries.
        /// </summary>
        /// <param name="x">The x coordinate.</param>
        /// <param name="y">The y coordinate.</param>
        /// <param name="c">The character to draw.</param>
        private static void PutCharacter(int x, int y, char c)
        {
            if (x < 0 || x >= Console.WindowWidth || y < 0 || y >= Console.WindowHeight)
            {
                return;
            }
            else
            {
                Console.SetCursorPosition(x, y);
                Console.Write(c);
            }
        }
    }
}
