﻿//-----------------------------------------------------------------------
// <copyright file="Program.cs" company="CompanyName">
//     Copyright (c) LettSoft.
// </copyright>
//-----------------------------------------------------------------------
namespace Aufgabe2
{
    using System;

    /// <summary>
    /// Main class of the programm.
    /// </summary>
    public class MainClass
    {
        /// <summary>
        /// The entry point of the program, where the program control starts and ends.
        /// </summary>
        /// <param name="args">The command-line arguments.</param>
        public static void Main(string[] args)
        {
            ConsoleColor oldForegroundColor = Console.ForegroundColor;
            ConsoleColor oldBackgroundColor = Console.BackgroundColor;

            WindowManager.ShowMainMenu();

            Console.ForegroundColor = oldForegroundColor;
            Console.BackgroundColor = oldBackgroundColor;
        }
    }
}
