﻿//-----------------------------------------------------------------------
// <copyright file="WindowManager.cs" company="CompanyName">
//     Copyright (c) LettSoft.
// </copyright>
//-----------------------------------------------------------------------
namespace Aufgabe2
{
    using System;

    /// <summary>
    /// Allows creation, manipulation and displaying of Window objects.
    /// </summary>
    public static class WindowManager
    {
        /// <summary>
        /// Displays the main menu of the window manager. Allows to create, edit, delete and display Window objects.
        /// </summary>
        public static void ShowMainMenu()
        {
            string[] mainMenuItems = 
            { 
                "Create Window", "Edit Window", "Delete Window", "Swap Windows", "Show All Windows",
                "Settings", "Create Random Windows", "Exit" 
            };
            string[] mainMenuDescriptions = 
            {
                "Create a new window", "Edit a window", "Delete a window", "Swap two windows",
                "Show all windows on screen", "Adjust settings", "Creates windows with random values", "Exit the program"
            };

            WindowList windows = new WindowList();

            Window defaultWindow = new Window();

            int chosenWindow;

            while (true)
            {
                int chosen = Utilities.ShowMenu("Main Menu", mainMenuItems, mainMenuDescriptions);

                switch (chosen)
                {
                    case 0: // Create new window
                        if (Utilities.AskForBoolean("Create new window?"))
                        {
                            windows.Add(EditWindow(new Window(defaultWindow), "Create New Window"));
                        }
                        break;
                    case 1: // Edit window
                        chosenWindow = SelectWindow(windows, "Edit Which Window?");
                        if (chosenWindow != -1)
                        {
                            Window window = windows.ElementAt(chosenWindow);
                            EditWindow(window, window.Title.Text);
                        }

                        break;
                    case 2: // Delete window
                        chosenWindow = SelectWindow(windows, "Delete Which Window?");
                        if (chosenWindow != -1)
                        {
                            if (Utilities.AskForBoolean("Are you sure you want to delete this window?"))
                            {
                                windows.RemoveAt(chosenWindow);
                            }

                            Console.WriteLine("Window has been deleted. Press Enter to continue.");
                            Console.ReadLine();
                        }

                        break;
                    case 3: // Swap windows
                        if (windows.Count < 2)
                        {
                            Console.WriteLine("ERROR: You need at least two windows to swap with.");
                            Console.WriteLine("Press enter to continue");
                            Utilities.WaitForKeys(ConsoleKey.Enter);
                            break;
                        }

                        int firstWindow = SelectWindow(windows, "Select The First Window");

                        if (firstWindow == -1)
                        {
                            break; // Quit if we canceled the first window selection
                        }

                        int secondWindow = SelectWindow(windows, "Select The Second Window", firstWindow);

                        if (secondWindow != -1)
                        {
                            if (Utilities.AskForBoolean($"Swap windows {firstWindow + 1} and {secondWindow + 1}?"))
                            {
                                windows.SwapElements(firstWindow, secondWindow);
                                Console.WriteLine("Windows have been swapped. Press Enter to continue.");
                                Console.ReadLine();
                            }
                        }

                        break;
                    case 4: // Show all windows
                        Console.Clear();
                        foreach (Window w in windows.ToArray())
                        {
                            WindowRenderer.DrawWindow(w);
                        }

                        Utilities.WaitForKeys(ConsoleKey.Escape, ConsoleKey.Q);
                        break;
                    case 5: // Settings
                        defaultWindow = EditWindow(defaultWindow, "Edit Default Settings");
                        break;
                    case 6: // Make random windows
                        int amount = Utilities.AskForNumber("How many windows do you want?", 1, 20);
                        Random r = new Random();

                        for (int i = 0; i < amount; i++)
                        {
                            windows.Add(CreateRandomWindow(r));
                        }

                        Console.WriteLine($"{amount} {(amount == 1 ? "window" : "windows")} created. Press Enter to continue");
                        Console.ReadLine();

                        break;
                    case 7: // Quit
                        return;
                }
            }
        }

        /// <summary>
        /// Creates and initalizes a Window with random values.
        /// </summary>
        /// <returns>The randomized window.</returns>
        /// <param name="r">An instance of Random.</param>
        private static Window CreateRandomWindow(Random r)
        {
            Window window = new Window();

            window.Height = r.Next(5, 20);
            window.Width = r.Next(7, 50);

            window.Position.X = r.Next(-5, 70);
            window.Position.Y = r.Next(-5, 20);

            window.Title.ForegroundColor = (ConsoleColor)r.Next(0, 16);
            window.Title.Backgroundcolor = (ConsoleColor)r.Next(0, 16);
            window.Title.Text = Utilities.GenerateRandomText(r, 2);

            window.Content.ForegroundColor = (ConsoleColor)r.Next(0, 16);
            window.Content.Backgroundcolor = (ConsoleColor)r.Next(0, 16);
            window.Content.Text = Utilities.GenerateRandomText(r, r.Next(5, 15));

            window.Border.ForegroundColor = (ConsoleColor)r.Next(0, 16);
            window.Border.Backgroundcolor = (ConsoleColor)r.Next(0, 16);
            window.Border.Character = (char)r.Next(65, 122);

            window.Shadow.ForegroundColor = (ConsoleColor)r.Next(0, 16);
            window.Shadow.Backgroundcolor = (ConsoleColor)r.Next(0, 16);
            window.Shadow.Character = (char)r.Next(65, 122);

            return window;
        }

        /// <summary>
        /// Displays an input mask for editing a Window object. The user can select a property and enter a new value for it.
        /// On exit, it returns the new window object.
        /// </summary>
        /// <returns>The edited Window.</returns>
        /// <param name="window">The Window to edit.</param>
        /// <param name="title">Title to display in the first line.</param>
        private static Window EditWindow(Window window, string title)
        {
            while (true)
            {
                string[] windowProperties = 
                {
                    $"Position",
                    $"├─Horizontal Position:\t{window.Position.X}",
                    $"└─Vertical Position:\t{window.Position.Y}",
                    $"Width:\t{window.Width}",
                    $"Height:\t{window.Height}",
                    $"Title:\t{window.Title.Text}",
                    $"├─Foreground Color: {window.Title.ForegroundColor}",
                    $"└─Background Color: {window.Title.Backgroundcolor}",
                    $"Content: {window.Content.Text}",
                    $"├─Foreground Color: {window.Content.ForegroundColor}",
                    $"└─Background Color: {window.Content.Backgroundcolor}",
                    $"Border: {window.Border.Character}",
                    $"├─Foreground Color: {window.Border.ForegroundColor}",
                    $"└─Background Color: {window.Border.Backgroundcolor}",
                    $"Shadow: {(window.Shadow.IsVisible ? "Enabled" : "Disabled")}",
                    $"├─Character: {window.Shadow.Character}",
                    $"├─Foreground Color: {window.Shadow.ForegroundColor}",
                    $"└─Background Color: {window.Shadow.Backgroundcolor}",
                    "Save and Exit"
                };

                int chosen = Utilities.ShowMenu(title, windowProperties);

                // Abandon all hope, ye who enter here
                switch (chosen)
                {
                    case 1: // X Pos
                        window.Position.X = Utilities.AskForNumber("Horizontal Position", -1000, 1000);
                        break;
                    case 2: // Y Pos
                        window.Position.Y = Utilities.AskForNumber("Vertical Position", -1000, 1000);
                        break;
                    case 3: // Window width
                        window.Width = Utilities.AskForNumber("Window Width", 7);
                        break;
                    case 4: // Window height
                        window.Height = Utilities.AskForNumber("Window Height", 5);
                        break;
                    case 5: // Title text
                        window.Title.Text = Utilities.AskForString("Window Title", window.Title.Text);
                        break;
                    case 6: // Title text foreground color
                        window.Title.ForegroundColor = Utilities.AskForColor("Window Title Foreground Color", window.Title.ForegroundColor);
                        break;
                    case 7: // Title text background color
                        window.Title.Backgroundcolor = Utilities.AskForColor("Window Title Background Color", window.Title.Backgroundcolor);
                        break;
                    case 8: // Content text
                        window.Content.Text = Utilities.AskForString("Window Content", window.Content.Text);
                        break;
                    case 9: // Content foreground color
                        window.Content.ForegroundColor = Utilities.AskForColor("Window Content Foreground Color", window.Content.ForegroundColor);
                        break;
                    case 10: // Content background color
                        window.Content.Backgroundcolor = Utilities.AskForColor("Window Content Background Color", window.Content.Backgroundcolor);
                        break;
                    case 11: // Border character
                        window.Border.Character = Utilities.AskForChar("Window Border Character");
                        break;
                    case 12: // Border foreground color
                        window.Border.ForegroundColor = Utilities.AskForColor("Window Border Foreground Color", window.Border.ForegroundColor);
                        break;
                    case 13: // Border background color
                        window.Border.Backgroundcolor = Utilities.AskForColor("Window Border Background Color", window.Border.Backgroundcolor);
                        break;
                    case 14: // Shadow enabled?
                        window.Shadow.IsVisible = Utilities.AskForBoolean("Enable window shadow?");
                        break;
                    case 15: // Shadow character
                        window.Shadow.Character = Utilities.AskForChar("Window Shadow Character");
                        break;
                    case 16: // Shadow Foreground color
                        window.Shadow.ForegroundColor = Utilities.AskForColor("Shadow Foreground Color", window.Shadow.ForegroundColor);
                        break;
                    case 17: // Shadow background color
                        window.Shadow.Backgroundcolor = Utilities.AskForColor("Shadow Background Color", window.Shadow.Backgroundcolor);
                        break;
                    case -1: // Save and quit
                    case 18:
                        if (Utilities.AskForBoolean("Save and Exit?"))
                        {
                            return window;
                        }

                        break;
                }
            }
        }

        /// <summary>
        /// Shows a list of windows and allow the user to select one of them.
        /// </summary>
        /// <returns>The index of the selected.</returns>
        /// <param name="list">An array of Window objects.</param>
        /// <param name="title">The title of the menu.</param>
        /// <param name="disabled">Optional. Grey out the chosen index.</param>
        private static int SelectWindow(WindowList list, string title, int disabled = -1)
        {
            return SelectWindow(list.ToArray(), title, disabled);
        }

        /// <summary>
        /// Shows a list of windows and allow the user to select one of them.
        /// </summary>
        /// <returns>The index of the selected.</returns>
        /// <param name="windows">An array of Window objects.</param>
        /// <param name="title">The title of the menu.</param>
        /// <param name="disabled">Optional. Grey out the chosen index.</param>
        private static int SelectWindow(Window[] windows, string title, int disabled = -1)
        {
            int index = 0;
            Window window;

            Console.CursorVisible = false;

            if (windows.Length == 0)
            {
                Console.WriteLine("No windows exist! Create a window first.");
                Console.WriteLine("Press enter to continue.");
                Utilities.WaitForKeys(ConsoleKey.Enter);
                return -1;
            }

            while (true)
            {
                Console.BackgroundColor = ConsoleColor.Black; // Make the console clear to black
                Console.Clear();

                Utilities.DrawTextBar(title, backgroundColor: ConsoleColor.Blue);
                Console.WriteLine();
                Console.WriteLine("Index | Title           | Left / Top | Width | Height");
                Console.WriteLine("------+-----------------+------------+-------+--------");

                for (int i = 0; i < windows.Length; i++)
                {
                    if (i == index)
                    {
                        Console.BackgroundColor = ConsoleColor.White;
                        Console.ForegroundColor = ConsoleColor.Black;
                    }

                    if (i == disabled)
                    {
                        Console.BackgroundColor = ConsoleColor.Gray;
                        Console.ForegroundColor = ConsoleColor.Black;
                    }

                    window = windows[i];

                    Console.Write($"{i + 1,5} |");
                    Console.Write($" {Utilities.TruncateString(window.Title.Text, 15),-15} |");
                    Console.Write($" {window.Position.X,5}/{window.Position.Y,-5}| ");
                    Console.Write($"{window.Width,6}| ");
                    Console.WriteLine($"{window.Height,7}");

                    Console.BackgroundColor = ConsoleColor.Black;
                    Console.ForegroundColor = ConsoleColor.White;
                }

                ConsoleKeyInfo input = Console.ReadKey(true);

                switch (input.Key)
                {
                    case ConsoleKey.DownArrow:
                        if (index < windows.Length - 1)
                        {
                            // Skip over disabled item
                            if (index + 1 == disabled) 
                            {
                                // Prevent the cursor from jumping out of the list
                                if (index + 2 < windows.Length) 
                                {
                                    index += 2;
                                }
                            }
                            else
                            {
                                index++;
                            }
                        }

                        break;
                    case ConsoleKey.UpArrow:
                        if (index > 0)
                        {
                            // Skip over disabled item
                            if (index - 1 == disabled) 
                            {
                                // Prevent the cursor from jumping out of the list
                                if (index - 2 >= 0) 
                                {
                                    index -= 2;
                                }
                            }
                            else
                            {
                                index--;
                            }
                        }

                        break;
                    case ConsoleKey.Enter:
                        return index;
                    case ConsoleKey.Escape:
                        return -1;
                }
            }
        }
    }
}
