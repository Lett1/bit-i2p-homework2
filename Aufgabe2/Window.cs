﻿//-----------------------------------------------------------------------
// <copyright file="Window.cs" company="CompanyName">
//     Copyright (c) LettSoft.
// </copyright>
//-----------------------------------------------------------------------
namespace Aufgabe2
{
    using System;
    using WindowComponents;

    /// <summary>
    /// Represents a single Window.
    /// </summary>
    public class Window
    {
        /// <summary>
        /// Position of the window starting in top left.
        /// </summary>
        private Vector2 position;

        /// <summary>
        /// Width of the window.
        /// </summary>
        private int width;

        /// <summary>
        /// Height of the window.
        /// </summary>
        private int height;

        /// <summary>
        /// The title of the window.
        /// </summary>
        private WindowText title;

        /// <summary>
        /// The content inside the window.
        /// </summary>
        private WindowText content;

        /// <summary>
        /// The border of the window.
        /// </summary>
        private WindowBorder border;

        /// <summary>
        /// The shadow of the window.
        /// </summary>
        private WindowShadow shadow;

        /// <summary>
        /// Initializes a new instance of the <see cref="T:Aufgabe2.Window"/> class.
        /// </summary>
        public Window()
        {
            this.Position = new Vector2(0, 0);
            this.Height = 5;
            this.Width = 20;

            this.Title = new WindowText("New Window");
            this.Content = new WindowText("Lorem ipsum dolor sit amet.");
            this.Border = new WindowBorder('#');

            this.Shadow = new WindowShadow(true, '▒');
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:Aufgabe2.Window"/> class, copying values from the supplied <see cref="T:Aufgabe2.Window"/> instance.
        /// </summary>
        /// <param name="baseWindow">Base Window.</param>
        public Window(Window baseWindow)
        {
            this.Position = new Vector2(baseWindow.Position);
            this.Height = baseWindow.Height;
            this.Width = baseWindow.Width;

            this.Title = new WindowText(baseWindow.Title);
            this.Content = new WindowText(baseWindow.Content);
            this.Border = new WindowBorder(baseWindow.Border);

            this.Shadow = new WindowShadow(baseWindow.Shadow);
        }

        /// <summary>
        /// Gets or sets the position of the window.
        /// </summary>
        /// <value>The position.</value>
        public Vector2 Position
        {
            get
            {
                return this.position;
            }

            set
            {
                this.position = value;
            }
        }

        /// <summary>
        /// Gets or sets the width. Cannot be smaller than 7.
        /// </summary>
        /// <value>The width.</value>
        public int Width 
        {
            get
            {
                return this.width;
            }

            set
            {
                if (value < 7)
                {
                    throw new ArgumentOutOfRangeException(nameof(value), "Width cannot be smaller than 7");
                }

                this.width = value;
            }
        }

        /// <summary>
        /// Gets or sets the height. Cannot be smaller than 5.
        /// </summary>
        /// <value>The height.</value>
        public int Height 
        {
            get
            {
                return this.height;
            }

            set
            {
                if (value < 5)
                {
                    throw new ArgumentOutOfRangeException(nameof(value), "Height cannot be smaller than 5");
                }

                this.height = value;
            }
        }

        /// <summary>
        /// Gets or sets the title of the window.
        /// </summary>
        /// <value>The title.</value>
        public WindowText Title 
        {
            get
            {
                return this.title;
            }

            set
            {
                this.title = value;
            }
        }

        /// <summary>
        /// Gets or sets the content in the window.
        /// </summary>
        /// <value>The content.</value>
        public WindowText Content 
        {
            get
            {
                return this.content;
            }

            set
            {
                this.content = value;
            }
        }

        /// <summary>
        /// Gets or sets the border of the window.
        /// </summary>
        /// <value>The border.</value>
        public WindowBorder Border 
        {
            get
            {
                return this.border;
            }

            set
            {
                this.border = value;
            }
        }

        /// <summary>
        /// Gets or sets the shadow of the window.
        /// </summary>
        /// <value>The shadow.</value>
        public WindowShadow Shadow 
        {
            get
            {
                return this.shadow;
            }

            set
            {
                this.shadow = value;
            }
        }
    }
}
